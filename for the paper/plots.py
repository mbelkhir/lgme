import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import math

from metrics import *

#latence = []
millis = []
card = []
req = []
ratio = []

millisY = []
cardY = []
reqY = []
ratioY = []

millisZ = []
cardZ = []
reqZ = []
ratioZ = []

millisQ = []
cardQ = []
reqQ = []
ratioQ = []

ms = []
cs = []

for i in range(0, len(leaveCS)):
    ms.append(leaveCS[i][0])
    cs.append(leaveCS[i][1])

for i in range(0, len(meX)):
    millis.append(meX[i][0])
    card.append(meX[i][2])
    req.append(meX[i][1])
    if meX[i][1] > 0:
        ratio.append(meX[i][2]/meX[i][1])
    else:
        ratio.append(1)

for i in range(0, len(meY)):
    millisY.append(meY[i][0])
    cardY.append(meY[i][2]*(-1))
    reqY.append(meY[i][1]*(-1))
    if meY[i][1] > 0:
        ratioY.append(meY[i][2]/meY[i][1])
    else:
        ratioY.append(1)

for i in range(0, len(meZ)):
    millisZ.append(meZ[i][0])
    cardZ.append(meZ[i][2])
    reqZ.append(meZ[i][1])
    if meZ[i][1] > 0:
        ratioZ.append(meZ[i][2]/meZ[i][1])
    else:
        ratioZ.append(1)

for i in range(0, len(meQ)):
    millisQ.append(meQ[i][0])
    cardQ.append(meQ[i][2])
    reqQ.append(meQ[i][1])
    if meQ[i][1] > 0:
        ratioQ.append(meQ[i][2]/meQ[i][1])
    else:
        ratioQ.append(1)
#
# millistosec = []

plt.plot(millis,req,'go--',label='#request',markersize=3)
plt.plot(millis,card,'ro--',label='card_CS',markersize=3)
plt.ylabel('#nodes of group X')
plt.xlabel('time (ms)')
plt.grid(linestyle=':',linewidth=0.5)
plt.legend(loc='upper right',prop={'size': 6})
plt.savefig('plotsX.pdf')

plt.figure()
plt.plot(millisY,reqY,'go--',label='#request',markersize=3)
plt.plot(millisY,cardY,'ro--',label='card_CS',markersize=3)
plt.ylabel('#nodes of group Y')
plt.xlabel('time (ms)')
plt.grid(linestyle=':',linewidth=0.5)
plt.legend(loc='upper right',prop={'size': 6})
plt.savefig('plotsY.pdf')

plt.figure()
plt.plot(millisY,ratioY,'go--',label='ratio',markersize=3)
plt.ylabel('efficiency of group Y')
plt.xlabel('time (ms)')
plt.grid(linestyle=':',linewidth=0.5)
plt.legend(loc='upper right',prop={'size': 6})
plt.savefig('ratio_plotsY.pdf')

plt.figure()
plt.plot(millis,ratio,'go--',label='ratio',markersize=3)
plt.ylabel('efficiency of group X')
plt.xlabel('time (ms)')
plt.grid(linestyle=':',linewidth=0.5)
plt.legend(loc='upper right',prop={'size': 6})
plt.savefig('ratio_plotsX.pdf')

plt.figure()
plt.plot(millisZ,ratioZ,'go--',label='ratio',markersize=3)
plt.ylabel('efficiency of group Z')
plt.xlabel('time (ms)')
plt.grid(linestyle=':',linewidth=0.5)
plt.legend(loc='upper right',prop={'size': 6})
plt.savefig('ratio_plotsZ.pdf')

plt.figure()
plt.plot(millisZ,reqZ,'go--',label='#request',markersize=3)
plt.plot(millisZ,cardZ,'ro--',label='card_CS',markersize=3)
plt.ylabel('#nodes of group X')
plt.xlabel('time (ms)')
plt.grid(linestyle=':',linewidth=0.5)
plt.legend(loc='upper right',prop={'size': 6})
plt.savefig('plotsZ.pdf')

#Group Q
plt.figure()
plt.plot(millisQ,ratioQ,'go--',label='ratio',markersize=3)
plt.ylabel('efficiency of group Q')
plt.xlabel('time (ms)')
plt.grid(linestyle=':',linewidth=0.5)
plt.legend(loc='upper right',prop={'size': 6})
plt.savefig('ratio_plotsQ.pdf')

plt.figure()
plt.plot(millisQ,reqQ,'go--',label='#request',markersize=3)
plt.plot(millisQ,cardQ,'ro--',label='card_CS',markersize=3)
plt.ylabel('#nodes of group Q')
plt.xlabel('time (ms)')
plt.grid(linestyle=':',linewidth=0.5)
plt.legend(loc='upper right',prop={'size': 6})
plt.savefig('plotsQ.pdf')

#2 plots
plt.figure()
plt.plot(millis,ratio,'go--',label='ratio X',markersize=3)
plt.plot(millisY,ratioY,'ro--',label='ratio Y',markersize=3)
plt.ylabel('efficiency')
plt.xlabel('time (ms)')
plt.grid(linestyle=':',linewidth=0.5)
plt.legend(loc='upper right',prop={'size': 6})
plt.savefig('ratio_plotsXY.pdf')

#two plots
plt.figure()
plt.plot(millis,req,'go--',label='#request nodes X',markersize=3)
plt.plot(millis,card,'ro--',label='card_CS node X',markersize=3)
plt.plot(millisY,reqY,'bo--',label='#request node Y',markersize=3)
plt.plot(millisY,cardY,'mo--',label='card_CS node Y',markersize=3)
plt.ylabel('#nodes')
plt.xlabel('time (ms)')
plt.grid(linestyle=':',linewidth=0.5)
plt.legend(loc='upper right',prop={'size': 6})
plt.savefig('plotssameXY.pdf')

#LEAVE_CS
plt.figure()
plt.plot(ms,cs,label='#leave CS',markersize=3)
plt.ylabel('#leave CS')
plt.xlabel('time (ms)')
plt.grid(linestyle=':',linewidth=0.5)
plt.legend(loc='upper right',prop={'size': 6})
plt.savefig('leaveCS.pdf')

#plt.show()
