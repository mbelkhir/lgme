package myapps;

import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;

public class KafkaParserSecondNode {

    public long startTime = new java.util.Date().getTime();
    //mettre le temps de référance +/- % id
    public Random random = new Random(startTime);
    //change ti min / max
    //0 et max (para)
    public int inCS;
    public int freqToRequestCS;
    public int tempOfSimulation = 10;

    public int id;

    public static String cluster;
    public String numThreads;

    //Variables used for scaling
    public boolean isLeader = true;
    public boolean isActive = true;

    //Variables used for GME
    public String state = null;
    public int LC = 0;
    public String target;
    public Priority req_id = new Priority(id, Integer.MAX_VALUE);
    public ArrayList<MessageGME> waiting_req = new ArrayList<>();
    public ArrayList<String> ack_recvd = new ArrayList<>();
    public ArrayList<String> ack_to_recvd;
    public ArrayList<String> aut_group = new ArrayList<>();
    public Integer nb_ack_to_recv;
    public ArrayList<String> acks_to_ignore = new ArrayList<>();

//    public KafkaParserSecondNode(int id, String group) {
//        this.id = id;
//
//        this.isLeader = true;
//        this.isActive = true;
//        this.cluster = "localhost:9092";
//        this.target = group;
//    }
//
//    public KafkaParserSecondNode(int id, String group, long startTime) {
//        this.id = id;
//
//        this.isLeader = true;
//        this.isActive = true;
//        this.cluster = "localhost:9092";
//        this.target = group;
//        this.startTime = startTime;
//    }
    public KafkaParserSecondNode(int id, String group, String startTimeint, int inCS, int freqToRequestCS) {
        this.id = id;

        this.isLeader = true;
        this.isActive = true;
        KafkaParserSecondNode.cluster = "localhost:9092";
        this.target = group;
        this.inCS = inCS;
        this.freqToRequestCS = freqToRequestCS;
        this.startTime = Long.parseLong(startTimeint);
    }

//    public KafkaParserSecondNode(int id, String group, int inCS, int freqToRequestCS) {
//        this.id = id;
//
//        this.isLeader = true;
//        this.isActive = true;
//        this.cluster = "localhost:9092";
//        this.target = group;
//        this.inCS = inCS;
//        this.freqToRequestCS = freqToRequestCS;
//    }
    public static void main(String[] args) {

        //System.out.println(seed);
        Object o = new Object();

        ProtocolGME protocol = new ProtocolGME();
        KafkaParserSecondNode node;
        Repertory repertory = new Repertory();

        switch (args.length) {
            case 0:
            //node = new KafkaParserSecondNode();
            case 1:
            //node = new KafkaParserSecondNode();
            //node = new KafkaParserSecondNode(int.fromString(args[0]));
            case 2:
            //node = new KafkaParserSecondNode(int.fromString(args[0]), args[1]);
            case 3:
            //node = new KafkaParserSecondNode(int.fromString(args[0]), args[1], Long.parseLong(args[2]));
            case 4:
            //node = new KafkaParserSecondNode(int.fromString(args[0]), args[1], Integer.parseInt(args[2]), Integer.parseInt(args[3]));
            case 5:
                node = new KafkaParserSecondNode(Integer.parseInt(args[0]), args[1], args[2], Integer.parseInt(args[3]), Integer.parseInt(args[4]));
                break;
            default:
                node = new KafkaParserSecondNode(Integer.parseInt(args[0]), args[1], args[2], Integer.parseInt(args[3]), Integer.parseInt(args[4]));
                //node = new KafkaParserSecondNode(args[0], args[1], int.fromString(args[2]));
                //node = new KafkaParserSecondNode();
                break;
        }
        switch (node.target) {
            case "Y":
                repertory.removeList(repertory.getSetY());
                node.ack_to_recvd = new ArrayList<>(repertory.getAllNodes());
                System.err.println(node.ack_to_recvd.toString());
                break;
            case "X":
                repertory.removeList(repertory.getSetX());
                node.ack_to_recvd = new ArrayList<>(repertory.getAllNodes());
                System.err.println(node.ack_to_recvd.toString());
                break;
            case "Z":
                repertory.removeList(repertory.getSetZ());
                node.ack_to_recvd = new ArrayList<>(repertory.getAllNodes());
                System.err.println(node.ack_to_recvd.toString());
                break;
            case "Q":
                repertory.removeList(repertory.getSetQ());
                node.ack_to_recvd = new ArrayList<>(repertory.getAllNodes());
                System.err.println(node.ack_to_recvd.toString());
                break;
            default:
                break;
        }

        /**
         * Producer's configuration for consumer (scaling algorithm)*
         */
        Properties scalingprops = new Properties();
        scalingprops.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, cluster);
        scalingprops.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        scalingprops.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonPOJOSerializer.class.getName());
        //scalingprops.put(ProducerConfig.ACKS_CONFIG, "0");
        Producer prodScaling = new KafkaProducer<>(scalingprops);

        ScalingConsumerThread scalingMsg = new ScalingConsumerThread(repertory, node, prodScaling, protocol, o);
        scalingMsg.start();

        RequestCS requestCS = new RequestCS(repertory, node, prodScaling, o);
        requestCS.start();
    }
}
