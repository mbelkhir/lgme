package myapps;

public class Priority {

    public int x;
    public int y;

    public Priority(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Boolean lessThan(Priority priority) {
        if (y < priority.y) {
            return true;
        } else if (y > priority.y) {
            return false;
        } else if (y == priority.y) {
            if (x < priority.x) {
                return true;
            } else if (x > priority.x) {
                return false;
            } else {
                System.err.println("Error & exit (two priority are equals");
                System.exit(0);
            }
        }
        return null;
    }
}
