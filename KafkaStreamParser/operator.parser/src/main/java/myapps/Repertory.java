package myapps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author mbelkhir
 */
public class Repertory {

    //private List<String> succs = new ArrayList<>(Arrays.asList("CK-a6b701ca-9c36-449c-bafe-de014da3e5ea"));
    private List<String> allNodes = new ArrayList<>(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"));
    private List<String> converge = new ArrayList<>(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"));
    private List<String> setX = new ArrayList<>(Arrays.asList("0", "1", "2", "3", "4", "5"));
    private List<String> setY = new ArrayList<>(Arrays.asList("6", "7", "8", "9", "10", "11"));
    private List<String> setZ = new ArrayList<>(Arrays.asList("12", "13", "14", "15", "16", "17"));
    private List<String> setQ = new ArrayList<>(Arrays.asList("18", "19", "20", "21", "22", "23"));

    public List<String> getAllNodes() {
        return allNodes;
    }

    public List<String> getSetQ() {
        return setQ;
    }

    public void setAllNodes(List<String> allNodes) {
        this.allNodes = allNodes;
    }

    public List<String> getSetX() {
        return setX;
    }

    public List<String> getConverge() {
        return converge;
    }

    public List<String> getSetY() {
        return setY;
    }

    public List<String> getSetZ() {
        return setZ;
    }

    public void removeList(List<String> list) {
        for (String id : list) {
            if (allNodes.contains(id)) {
                allNodes.remove(id);
            }
        }
    }
}
