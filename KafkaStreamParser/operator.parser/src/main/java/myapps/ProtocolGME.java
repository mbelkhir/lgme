package myapps;

import static java.lang.Integer.max;
import java.util.Iterator;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author mbelkhir
 */
public class ProtocolGME {

    public void onReceiptOf(MessageGME msg, Repertory repertory, KafkaParserSecondNode node, Producer producer, Object ob) {
        synchronized (ob) {
            switch (msg.getType()) {
                case "REQ": {
//                    System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
//                            + " id:" + node.id + " RCV:REQ FROM:" + msg.getSrc()
//                            + " LC_i'j' = " + msg.getLC() + " req_id:" + node.req_id.y + " end");
                    Priority msgPriority = new Priority(Integer.parseInt(msg.getSrc()), msg.getLC());
                    node.LC = max(msg.getLC(), node.LC);
                    if (msgPriority.lessThan(node.req_id)) {
                        //System.out.println("#SEND end");
                        System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                                + " id:" + node.id + " SEND:ACK_FALSE(-1) TO:" + msg.getSrc() + " end");
                        ProducerRecord<String, MessageGME> rec = new ProducerRecord<>(msg.getSrc(),
                                new MessageGME(node.target, null, msg.getSrc(), node.id + "", "ACK", -1));
                        producer.send(rec);
                        if (!node.aut_group.contains(msg.getTarget())) {
                            node.aut_group.add(msg.getTarget());
//                            System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
//                                    + " id:" + node.id + " ADD:AUT_GROUP:" + msg.getTarget() + " aut_group:"
//                                    + node.aut_group.toString()+ " end");
                            Iterator<MessageGME> itr = node.waiting_req.iterator();
                            while (itr.hasNext()) {
                                MessageGME nik = itr.next();
                                if (nik.getTarget().equals(msg.getTarget())) {
                                    if (node.ack_recvd.contains(nik.getSrc())) {
                                        System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                                                + " id:" + node.id + " REMOVE:ACK FROM:" + nik.getSrc() + " ack_to_received:" + node.ack_to_recvd + " end");
                                        node.nb_ack_to_recv++;
                                        node.ack_recvd.remove(nik.getSrc());
                                        node.ack_to_recvd.add(nik.getSrc());
                                    } else {
                                        System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime) + " id:" + node.id + " STORE:ACK_IGNORE FROM:" + nik.getSrc() + " end");
                                        node.acks_to_ignore.add(nik.getSrc());
                                    }
                                    System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime) + " id:" + node.id + " SEND:ACK_TRUE_(n_ik ∈ waiting req)(0) TO:" + nik.getSrc() + " end");
                                    //System.out.println("#SEND end");
                                    ProducerRecord<String, MessageGME> p_nik = new ProducerRecord<>(nik.getSrc(),
                                            new MessageGME(node.target, null, nik.getSrc(), node.id + "", "ACK", 0));
                                    producer.send(p_nik);
                                    itr.remove();
                                }
                            }
                        }
                    } else {
                        if (node.aut_group.contains(msg.getTarget())) {
                            if (node.ack_recvd.contains(msg.getSrc())) {
                                node.nb_ack_to_recv++;
                                node.ack_recvd.remove(msg.getSrc());
                                node.ack_to_recvd.add(msg.getSrc());
                                System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                                        + " id:" + node.id + " REMOVE:ACK FROM:" + msg.getSrc() + " ack_to_received:" + node.ack_to_recvd + " end");
                            } else { //Vérifier ligne 41
                                //acks to ignore ← acks to ignore ∪ {ni'j'}
                                System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                                        + " id:" + node.id + " STORE:ACK_IGNORE FROM:" + msg.getSrc() + " end");
                                node.acks_to_ignore.add(msg.getSrc());
                            }
                            //System.out.println("#SEND end");
                            System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                                    + " id:" + node.id + " SEND:ACK_TRUE_(i' ∈ aut_groups)(0) TO:" + msg.getSrc() + " end");
                            ProducerRecord<String, MessageGME> ack = new ProducerRecord<>(msg.getSrc(),
                                    new MessageGME(node.target, null, msg.getSrc(), node.id + "", "ACK", 0));
                            producer.send(ack);
                        } else {

                            if (!node.waiting_req.contains(new MessageGME(msg.getTarget(), null, msg.getDest(), msg.getSrc(), "REQ", null))) {
//                                System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
//                                        + " id:" + node.id + " STORE:REQ_(waiting_req) FROM:" + msg.getSrc() + " end");
                                node.waiting_req.add(new MessageGME(msg.getTarget(), null, msg.getDest(), msg.getSrc(), "REQ", null));
                            }
                        }
                    }
                    break;
                }
                case "ACK": {
                    System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                            + " id:" + node.id + " RCV:ACK FROM:" + msg.getSrc() + " ack_to_received:" + node.ack_to_recvd + " end");
                    if (node.acks_to_ignore.contains(msg.getSrc())) {
                        System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                                + " id:" + node.id + " IGNORE:ACK_(after receive) FROM:" + msg.getSrc() + " end");
                        node.acks_to_ignore.remove(msg.getSrc());
                    } else {
                        if (msg.getRenew() == 1) {
                            node.aut_group.remove(msg.getTarget());
//                            System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
//                                    + " id:" + node.id + " REMOVE:AUT_GROUP:" + msg.getTarget()
//                                    + " aut_group:" + node.aut_group.toString() + " end");

                        }
                        node.nb_ack_to_recv--;
                        node.ack_recvd.add(msg.getSrc());
                        node.ack_to_recvd.remove(msg.getSrc());
                        if (msg.getRenew() == 0) {
                            if (!node.waiting_req.contains(new MessageGME(msg.getTarget(), null, msg.getDest(), msg.getSrc(), "REQ", null))) {
//                                System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
//                                        + " id:" + node.id + " STORE:REQ_(waiting_req)(receive_ack_renew) FROM:" + msg.getSrc() + " end");
                                node.waiting_req.add(new MessageGME(msg.getTarget(), null, msg.getDest(), msg.getSrc(), "REQ", null));
                            }
                            node.aut_group.remove(msg.getTarget());
//                            System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
//                                    + " id:" + node.id + " REMOVE:AUT_GROUP(if_renew):" + msg.getTarget()
//                                    + " size:" + node.aut_group.size() + " end");
                        }
                        if (node.nb_ack_to_recv == 0) {
                            node.state = "in_cs";
                            System.out.print("time:" + (new java.util.Date().getTime() - node.startTime)
                                    + " id:" + node.id + " state:ENTER_CS " + "Group:" + node.target + " end");
                            ob.notify();
                        }
                    }
                    break;
                }
                case "END": {
//                    System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
//                            + " id:" + node.id + " RCV:END FROM:" + msg.getSrc() + " end");
                    node.aut_group.remove(msg.getTarget());
//                    System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
//                            + " id:" + node.id + " REMOVE:AUT_GROUP:" + msg.getTarget()
//                            + " aut_groupe:" + node.aut_group.toString() + " end");

                    break;
                }
                default:
                    //System.err.println("message not recognized");
                    break;
            }
        }
    }
}
