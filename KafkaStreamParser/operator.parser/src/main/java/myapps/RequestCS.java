package myapps;

import java.util.ArrayList;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author mbelkhir
 */
public class RequestCS extends Thread {

    Repertory repertory;
    KafkaParserSecondNode node;
    Producer producer;
    Object o;
    Integer temp = 0;

    public RequestCS(Repertory repertory, KafkaParserSecondNode node, Producer producer, Object ob) {
        this.repertory = repertory;
        this.node = node;
        this.producer = producer;
        o = ob;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5000);
            repertory.getConverge().stream().map((succ) -> {
                ProducerRecord<String, MessageGME> rec = new ProducerRecord<>(succ,
                        new MessageGME(null, null, null, null, "Hi", null));
                return rec;
            }).forEachOrdered((rec) -> {
                producer.send(rec);
            });
            Thread.sleep(2000);
            while (true) {
                Thread.sleep(node.freqToRequestCS);
                if (temp >= node.tempOfSimulation) {
                    //send msg end
                    System.out.println("#finished");
                    sleep(20000);
                    System.exit(0);
                }
                synchronized (o) {

                    if (node.state == null) {
                        temp++;
                        //if (!node.state.equals("in_cs") || !node.state.equals("requesting")) {
                        node.state = "requesting";
                        node.LC++;
                        node.req_id = new Priority(node.id, node.LC);
                        node.nb_ack_to_recv = repertory.getAllNodes().size();
                        node.ack_recvd = new ArrayList<>();
                        node.ack_to_recvd = new ArrayList<>(repertory.getAllNodes());
                        System.out.println("time:" + (new java.util.Date().getTime() - node.startTime) + " id:" + node.id + " state:REQ " + "Group:" + node.target + " LC:" + node.LC + " end");
//                                "Group:" + node.target + " id:" + node.id + " state:REQ time:"
//                                + (new java.util.Date().getTime() - node.startTime) + " ms LC = " + node.LC + " ms\n");
                        repertory.getAllNodes().stream().map((succ) -> {
                            //System.out.println("#SEND end");
                            ProducerRecord<String, MessageGME> rec = new ProducerRecord<>(succ,
                                    new MessageGME(node.target, node.LC, succ, node.id + "", "REQ", null));
                            return rec;
                        }).forEachOrdered((rec) -> {
                            producer.send(rec);
                        });
                    }
                    o.wait();
                }
                System.err.println("node" + node.id + ":" + temp);
                Thread.sleep(node.inCS);
                synchronized (o) {
                    System.out.println("time:" + (new java.util.Date().getTime() - node.startTime) + " id:" + node.id + " state:LEAVE_CS " + "Group:" + node.target + " end");
                    node.state = null;
                    node.req_id = new Priority(node.id, Integer.MAX_VALUE);
                    node.waiting_req.stream().map((reqSet) -> {
                        if (!node.aut_group.contains(reqSet.getTarget())) {
                            node.aut_group.add(reqSet.getTarget());
//                            System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
//                                    + " id:" + node.id + " ADD:AUT_GROUP:" + reqSet.getTarget() + " size:" + node.aut_group.size() + " end");
                        }
                        //System.out.println("#SEND end");
                        System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                                + " id:" + node.id + " SEND:ACK_AFTER_LEAVING_CS TO:" + reqSet.getSrc() + " end");
                        ProducerRecord<String, MessageGME> rec = new ProducerRecord<>(reqSet.getSrc(),
                                new MessageGME(node.target, null, reqSet.getSrc(), node.id + "", "ACK", 1));
//                        
                        return rec;
                    }).forEachOrdered((rec) -> {
                        producer.send(rec);
                        //System.out.println("Parser Send ACK from waiting req");
                    });
                    //multicast End(Nij) to competitors \ waiting req
                    ArrayList<String> aux_waiting_req = new ArrayList<>();
                    for (MessageGME string : node.waiting_req) {
                        aux_waiting_req.add(string.getSrc());
                    }
                    for (String neighbor : repertory.getAllNodes()) {
                        if (!aux_waiting_req.contains(neighbor)) {
                            //System.out.println("#SEND end");
                            ProducerRecord<String, MessageGME> end = new ProducerRecord<>(neighbor,
                                    new MessageGME(node.target, null, neighbor, node.id + "", "END", null));
                            producer.send(end);
//                            System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
//                                    + " id:" + node.id + " SEND:END TO:" + neighbor + " end");
                        }
                    }
                    node.waiting_req = new ArrayList<>();
                }

            }
        } catch (InterruptedException ex) {
        }
    }
}
