package myapps;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class MessageGME {

    //Variables used to be send in REQ & ACK for GME messages   
    private String target;
    private Integer LC;
    private String dest;
    private String src;
    private String type;
    private Integer renew;

    public MessageGME() {
    }

    public MessageGME(String target, Integer LC, String dest, String src, String type, Integer renew) {
        this.target = target;
        this.LC = LC;
        this.dest = dest;
        this.src = src;
        this.type = type;
        this.renew = renew;
    }

    public String getTarget() {
        return target;
    }

    public Integer getLC() {
        return LC;
    }

    public String getDest() {
        return dest;
    }

    public String getSrc() {
        return src;
    }

    public String getType() {
        return type;
    }

    public Integer getRenew() {
        return renew;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public void setLC(Integer LC) {
        this.LC = LC;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setRenew(Integer renew) {
        this.renew = renew;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        MessageGME guest = (MessageGME) obj;
        return (Objects.equals(LC, guest.LC) || (LC != null && LC.equals(guest.getLC())))
                && ((target == null ? guest.target == null : target.equals(guest.target)) || (target != null && target.equals(guest.getTarget())))
                && ((dest == null ? guest.dest == null : dest.equals(guest.dest)) || (dest != null && dest.equals(guest.getDest())))
                && ((src == null ? guest.src == null : src.equals(guest.src)) || (src != null && src.equals(guest.getSrc())))
                && ((type == null ? guest.type == null : type.equals(guest.type)) || (type != null && type.equals(guest.getType())))
                && (Objects.equals(renew, guest.renew) || (renew != null && renew.equals(guest.getRenew())));
    }

    @Override
    public String toString() {
        return "MessageGME{" + "target=" + target + ", LC=" + LC + ", dest=" + dest + ", src=" + src + ", type=" + type + ", renew=" + renew + '}';
    }
}
